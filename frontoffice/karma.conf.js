module.exports = function(config) {
  config.set({
    basePath: './',
    autoWatch: true,
    frameworks: ['mocha'],
    files: [
      'test/*.js',
      'test/**/*.js'
    ],
    browsers: ['Firefox',],
    plugins: [
        'karma-mocha',
        'karma-phantomjs-launcher',
        'karma-coverage',
        'karma-firefox-launcher',
    ],
    reporters: ['progress', 'coverage'],
    preprocessors: { '*.js': ['coverage'] },
    logLevel: config.LOG_ERROR,
    singleRun: true,
    port: 9999,
    colors: true,
    coverageReporter: {
        type : 'html',
        dir : 'coverage/'
    }
  });
};
