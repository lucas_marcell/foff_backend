'use strict';

module.exports = function(grunt) {

    grunt.initConfig({

        karma: {
            test: {
                configFile: 'karma.conf.js'
            }
        },
        less: {
            development : {
                options: {
                    compress: true,
                    yuicompress: true,
                    optimization: 2,
                },
                files: {
                    'public/stylesheets/css/style.css': 'public/stylesheets/less/style.less'
                }
            }
        },
        watch: {
            styles: {
                files: [
                    'public/stylesheets/less/style.less'
                ],
                tasks: ['less'],
                options: {
                    nospawn: true
                    livereload: true
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-karma');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.registerTask('default', ['watch']);
};
