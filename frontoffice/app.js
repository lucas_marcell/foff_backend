
/**
 * Module dependencies.
 */

const SECRET='AS(D*AS()D*as90d8as90f8sd90fg8d90f8g7d8f9789234234jkn234j2k',
    KEY='',
    MAX_AGE = {maxAge: 3600000},
    GZIP_LVL = {level:9, memLevel:9};

var express = require('express'),
    load = require('express-load'),
    http = require('http'),
    path = require('path'),
    mongoose = require('mongoose')
    app = express();


global.db = mongoose.connect(
    'mongodb://user:pass@ip:port/db_name',
    function(err, res) {
        if (err) {
            console.log ('ERROR connecting to: ' +  err);
        } else {
            console.log ('SUCCEEDED connecting to: ');
        }

    }
);

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.cookieParser(SECRET));
app.use(express.session());
app.use(express.favicon());
app.use(express.json());
app.use(express.logger('dev'));
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

app.use(function(req, res, next) {
    res.status(404);
    res.render('404');
});

app.use(function(error, req, res, next) {
    res.status(500);
    console.log(error);
    res.render(
        '500',
        {
            title: '500: Internal Server Error',
            error: error
        }
    );
});


// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

load('models').then('controllers').then('routes').into(app);

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});

module.exports = app;
