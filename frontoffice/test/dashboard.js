var app = require('../app'),
    should = require('should'),
    request = require('supertest')(app);

describe('No Controller Dashboard', function(){
    it('Deve retornar status 200 ao fazer GET /dashboard', function(done) {
        request.get('/dashboard').end(function(err, res) {
            res.status.should.eql(200);
            done();
        });
    });
    
});
