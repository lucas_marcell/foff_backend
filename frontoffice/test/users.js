var app = require('../app'),
    should = require('should'),
    request = require('supertest')(app);

describe('Rotas no Controller User', function(){
    it('Deve retornar status 200 ao fazer GET /users', function(done) {
        request.get('/users').end(function(err, res) {
            res.status.should.eql(200);
            done();
        });
    });
    it('Deve retornar status 200 ao fazer GET /users/id', function(done) {
        request.get('/users/id').end(function(err, res) {
            res.status.should.eql(200);
            done();
        });
    });
    it('Deve retornar status 200 ao fazer GET /users/create', function(done) {
        request.get('/users/create').end(function(err, res) {
            res.status.should.eql(200);
            done();
        });
    });
});
