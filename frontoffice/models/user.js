module.exports = function(app) {
    var Schema = require('mongoose').Schema;

    var user = Schema({
        first_name: {
            type: String,
            required: true
        },
        last_name: {
            type: String,
            required: true
        },
        username: {
            type: String,
            required: true
        },
        password: {
            type: String,
            required: true
        },
        email: {
            type: String,
            required: true,
            index: {
                unique: true
            }
        },
        created: {
            type: Date,
            default: Date.now,
        },

    });

    return db.model('users', user);
}
