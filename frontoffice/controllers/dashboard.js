module.exports = function(app) {
    /**
     * Define o controller Home
     */
    var DashboardController = {

        /**
         * Define a função para 
         * renderizar a pagina incial
         */
        index: function (req, res, next) {
            var user = req.session.user,
                params = {
                    user: user,
                };
            res.render('dashboard/index', params);
        },
    };
    return DashboardController;
}
