module.exports = function(app) {
    /**
     * Define o model de usuario
     */
    var User = app.models.users;

    /**
     * Define o controller Home
     */
    var HomeController = {

        /**
         * Define a função para 
         * renderizar a pagina incial
         */
        index: function (req, res, next) {
            res.render('home/index');
        },
        /**
         * Define a função para realizar
         * o login no sistema
         */
        login: function (req, res, next) {
            res.render('home/index');
        },
        /**
         * Define a função para realizar
         * o logout no sistema
         */
        logout: function (req, res, next) {
            res.render('home/index');
        },
    };
    return HomeController;
}
