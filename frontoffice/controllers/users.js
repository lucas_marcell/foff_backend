module.exports = function(app) {
    /**
     * Define o model de usuario
     */
    var User = app.models.user;

    /**
     * Define o controller Home
     */
    var UserController = {

        /**
         * Define a função para 
         * renderizar a pagina incial
         */
        index: function (req, res, next) {
                res.render(
                    'users/index',
                    {
                        users: []
                    }
                );
        },
        /**
         * Define a função para 
         * exibir detalhes do usuario
         */
        show: function (req, res, next) {
            res.render('home/index');
        },
        /**
         * Define a função para 
         * renderizar o formulario de cadastro
         * do usuario
         */
        create: function (req, res, next) {
            res.render('users/create');
        },
        /**
         * Define a função para 
         * renderizar a pagina incial
         */
        insert: function (req, res, next) {
            res.render('home/index');
        },
        /**
         * Define a função para 
         * renderizar a pagina incial
         */
        edit: function (req, res, next) {
            res.render('home/index');
        },
        /**
         * Define a função para 
         * renderizar a pagina incial
         */
        update: function (req, res, next) {
            res.render('home/index');
        },
        /**
         * Define a função para 
         * renderizar a pagina incial
         */
        destroy: function (req, res, next) {
            res.render('home/index');
        },
    };
    return UserController;
}
