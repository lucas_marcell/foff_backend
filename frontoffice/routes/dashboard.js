

/*
 * GET home page.
 */

module.exports = function(app) {
    var dashboard = app.controllers.dashboard;
    app.get('/dashboard', dashboard.index);
};
