module.exports = function(app) {
    var user = app.controllers.users;
    app.get('/users', user.index);
    app.get('/users/:id', user.show);
    app.get('/users/create', user.create)
    app.post('/users/insert', user.insert);
    app.get('/users/:id/edit', user.edit);
    app.put('/users/:id', user.update);
    app.del('/users/:id', user.destroy);
};
